# Pokédex

A simple Pokédex web app using [PokéAPI](https://pokeapi.co/). This webapp is maintained
by Brian Douglass and is not affiliated with or endorsed by Pokémon or Nintendo.
Pokémon and Pokémon character names are trademarks of Nintendo.

## Icons

Pokéball icon made by [Those Icons](https://www.flaticon.com/authors/those-icons)
from [www.flaticon.com](https://www.flaticon.com/) and is licensed by
[CC 3.0 BY](http://creativecommons.org/licenses/by/3.0/).


## License

Copyright (C) 2019 [Brian Douglass](http://bhdouglass.com/)

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License version 3, as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranties of MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.
