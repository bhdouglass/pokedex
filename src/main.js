import 'vanilla-framework/scss/build.scss';

import Vue from 'vue';
import VueLazyload from 'vue-lazyload'
import VueHead from 'vue-head'

import App from './App.vue';
import router from './router';
import {titleCase} from './helpers';

Vue.config.productionTip = false;

Vue.use(VueLazyload);
Vue.use(VueHead, {
    separator: '-',
    complement: 'Pokédex'
});

Vue.filter('titleCase', titleCase);

new Vue({
    router,
    render: (h) => h(App),
}).$mount('#app');
