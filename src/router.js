import Vue from 'vue';
import Router from 'vue-router';
import Versions from './views/Versions.vue';
import Pokedex from './views/Pokedex.vue';
import Pokemon from './views/Pokemon.vue';
import Types from './views/Types.vue';

Vue.use(Router);

export default new Router({
    mode: 'history',
    base: process.env.BASE_URL,
    routes: [
        {
            path: '/',
            name: 'versions',
            component: Versions,
        },
        {
            path: '/pokedex/:group',
            name: 'pokedex',
            component: Pokedex,
        },
        {
            path: '/pokedex/:group/:name',
            name: 'pokemon',
            component: Pokemon,
        },
        {
            path: '/types',
            name: 'types-default',
            component: Types,
        },
        {
            path: '/types/:gen',
            name: 'types',
            component: Types,
        },
    ],
});
