export function lang(list, lang = 'en') {
    return list.find((value) => value.language.name == lang);
}

export function transformVersion(version) {
    return {
        name: version.name,
        group: version.version_group.name,
        title: lang(version.names).name,
    };
}

export function titleCase(str) {
    return str.replace(/-/g, ' ').replace(/\w\S*/g, (s) => s.charAt(0).toUpperCase() + s.substr(1).toLowerCase());
}
